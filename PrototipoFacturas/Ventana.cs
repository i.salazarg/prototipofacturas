﻿using System;
using System.Windows.Forms;

namespace Facturas.Presentacion
{
    public partial class Ventana : Form
    {
        public Ventana()
        {
            InitializeComponent();

            CboxFiltro.Items.Add("Sin Filtro");
            CboxFiltro.Items.Add("Filtrar por Codigo");
            CboxFiltro.Items.Add("Filtrar por Fecha");
            CboxFiltro.Items.Add("Filtrar por Tipo");
            CboxFiltro.Items.Add("Filtrar por Surcursal");
            CboxFiltro.SelectedIndex = 0;

            DTimeBusqueda.MaxDate = DateTime.Today;
            DTimeBusqueda.Value = DateTime.Today;
        }

        private void CboxFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Selection = CboxFiltro.SelectedIndex;
            bool IsNoFilter = Selection == 0;
            bool IsDateFilter = Selection == 2;

            TboxBusqueda.Enabled = !IsNoFilter & !IsDateFilter;
            TboxBusqueda.Visible = !IsDateFilter;
            
            DTimeBusqueda.Enabled = IsDateFilter;
            DTimeBusqueda.Visible = IsDateFilter;

            BtnBuscar.Enabled = !IsNoFilter;
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {

        }
    }
}
