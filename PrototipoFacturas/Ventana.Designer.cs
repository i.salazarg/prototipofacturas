﻿namespace Facturas.Presentacion
{
    partial class Ventana
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TboxBusqueda = new System.Windows.Forms.TextBox();
            this.LblFiltro = new System.Windows.Forms.Label();
            this.CboxFiltro = new System.Windows.Forms.ComboBox();
            this.LblBusqueda = new System.Windows.Forms.Label();
            this.BtnBuscar = new System.Windows.Forms.Button();
            this.DgridLista = new System.Windows.Forms.DataGridView();
            this.CodigoVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaVenta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodigoSucursal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTimeBusqueda = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.DgridLista)).BeginInit();
            this.SuspendLayout();
            // 
            // TboxBusqueda
            // 
            this.TboxBusqueda.AccessibleDescription = "Cuadro de texto para ingresar la venta a buscar";
            this.TboxBusqueda.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.TboxBusqueda.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TboxBusqueda.Location = new System.Drawing.Point(320, 72);
            this.TboxBusqueda.Name = "TboxBusqueda";
            this.TboxBusqueda.Size = new System.Drawing.Size(432, 32);
            this.TboxBusqueda.TabIndex = 3;
            // 
            // LblFiltro
            // 
            this.LblFiltro.AutoSize = true;
            this.LblFiltro.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFiltro.Location = new System.Drawing.Point(80, 40);
            this.LblFiltro.Name = "LblFiltro";
            this.LblFiltro.Size = new System.Drawing.Size(140, 23);
            this.LblFiltro.TabIndex = 0;
            this.LblFiltro.Text = "Filtro a Utilizar";
            // 
            // CboxFiltro
            // 
            this.CboxFiltro.AccessibleDescription = "Listado de seleccion del filtro a utilizar";
            this.CboxFiltro.AccessibleRole = System.Windows.Forms.AccessibleRole.ComboBox;
            this.CboxFiltro.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CboxFiltro.FormattingEnabled = true;
            this.CboxFiltro.Location = new System.Drawing.Point(80, 72);
            this.CboxFiltro.Name = "CboxFiltro";
            this.CboxFiltro.Size = new System.Drawing.Size(216, 31);
            this.CboxFiltro.TabIndex = 1;
            this.CboxFiltro.SelectedIndexChanged += new System.EventHandler(this.CboxFiltro_SelectedIndexChanged);
            // 
            // LblBusqueda
            // 
            this.LblBusqueda.AutoSize = true;
            this.LblBusqueda.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBusqueda.Location = new System.Drawing.Point(320, 40);
            this.LblBusqueda.Name = "LblBusqueda";
            this.LblBusqueda.Size = new System.Drawing.Size(108, 23);
            this.LblBusqueda.TabIndex = 2;
            this.LblBusqueda.Text = "Busqueda";
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.AccessibleDescription = "Botón de busqueda";
            this.BtnBuscar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.BtnBuscar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBuscar.Location = new System.Drawing.Point(776, 72);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(136, 32);
            this.BtnBuscar.TabIndex = 5;
            this.BtnBuscar.Text = "Buscar";
            this.BtnBuscar.UseVisualStyleBackColor = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // DgridLista
            // 
            this.DgridLista.AccessibleDescription = "Tabla de resultados de la busqueda";
            this.DgridLista.AccessibleRole = System.Windows.Forms.AccessibleRole.Table;
            this.DgridLista.AllowUserToAddRows = false;
            this.DgridLista.AllowUserToDeleteRows = false;
            this.DgridLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgridLista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodigoVenta,
            this.FechaVenta,
            this.TipoDocumento,
            this.CodigoSucursal});
            this.DgridLista.Location = new System.Drawing.Point(80, 128);
            this.DgridLista.Name = "DgridLista";
            this.DgridLista.ReadOnly = true;
            this.DgridLista.RowTemplate.Height = 24;
            this.DgridLista.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgridLista.Size = new System.Drawing.Size(832, 544);
            this.DgridLista.StandardTab = true;
            this.DgridLista.TabIndex = 6;
            // 
            // CodigoVenta
            // 
            this.CodigoVenta.HeaderText = "Codigo Venta";
            this.CodigoVenta.Name = "CodigoVenta";
            this.CodigoVenta.ReadOnly = true;
            this.CodigoVenta.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CodigoVenta.Width = 145;
            // 
            // FechaVenta
            // 
            this.FechaVenta.HeaderText = "Fecha Venta";
            this.FechaVenta.Name = "FechaVenta";
            this.FechaVenta.ReadOnly = true;
            this.FechaVenta.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FechaVenta.Width = 145;
            // 
            // TipoDocumento
            // 
            this.TipoDocumento.HeaderText = "Tipo Documento";
            this.TipoDocumento.Name = "TipoDocumento";
            this.TipoDocumento.ReadOnly = true;
            this.TipoDocumento.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TipoDocumento.Width = 145;
            // 
            // CodigoSucursal
            // 
            this.CodigoSucursal.HeaderText = "Codigo Surcursal";
            this.CodigoSucursal.Name = "CodigoSucursal";
            this.CodigoSucursal.ReadOnly = true;
            this.CodigoSucursal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CodigoSucursal.Width = 145;
            // 
            // DTimeBusqueda
            // 
            this.DTimeBusqueda.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTimeBusqueda.Location = new System.Drawing.Point(320, 72);
            this.DTimeBusqueda.Name = "DTimeBusqueda";
            this.DTimeBusqueda.Size = new System.Drawing.Size(432, 32);
            this.DTimeBusqueda.TabIndex = 4;
            // 
            // Ventana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.DTimeBusqueda);
            this.Controls.Add(this.DgridLista);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.LblBusqueda);
            this.Controls.Add(this.CboxFiltro);
            this.Controls.Add(this.LblFiltro);
            this.Controls.Add(this.TboxBusqueda);
            this.Name = "Ventana";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Facturas";
            ((System.ComponentModel.ISupportInitialize)(this.DgridLista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TboxBusqueda;
        private System.Windows.Forms.Label LblFiltro;
        private System.Windows.Forms.ComboBox CboxFiltro;
        private System.Windows.Forms.Label LblBusqueda;
        private System.Windows.Forms.Button BtnBuscar;
        private System.Windows.Forms.DataGridView DgridLista;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigoVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaVenta;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodigoSucursal;
        private System.Windows.Forms.DateTimePicker DTimeBusqueda;
    }
}

