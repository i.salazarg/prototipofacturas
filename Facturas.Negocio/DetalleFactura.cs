﻿namespace Facturas.Negocio
{
    public class DetalleFactura
    {
        public int CodigoVenta { get; set; }
        public string NombreEjemplar { get; set; }
        public int Cantidad { get; set; }
        public int Valor { get; set; }
    }
}