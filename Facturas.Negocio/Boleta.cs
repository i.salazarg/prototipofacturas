﻿namespace Facturas.Negocio
{
    public class Boleta : Venta
    {
        public int CodigoBoleta { get; set; }
        public int Precio { get; set; }
    }
}