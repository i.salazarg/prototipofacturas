﻿namespace Facturas.Negocio
{
    public class Cliente
    {
        public int Codigo { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Comuna { get; set; }
        public string Telefono { get; set; }
    }
}