﻿namespace Facturas.Negocio
{
    public class Factura : Venta
    {
        public int CodigoCliente { get; set; }
        public int ValorNeto { get; set; }
        public int ImpuestoIva { get; set; }
        public int ValorTotal { get; set; }
    }
}