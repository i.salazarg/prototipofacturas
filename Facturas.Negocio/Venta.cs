﻿using System;

namespace Facturas.Negocio
{
    public class Venta
    {
        public int CodigoVenta { get; set; }
        public DateTime Fecha { get; set; }
        public string Sucursal { get; set; }
        public string TipoDocumento { get; set; }
    }
}