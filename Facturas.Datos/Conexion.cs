﻿using Oracle.ManagedDataAccess.Client;

namespace Facturas.Datos
{
    public class Conexion
    {
        private OracleConnection DBConnection;
        private string Credentials = string.Format("Data Source={0};User Id={1};Password={2};", "localhost", "facturas1", "facturas");

        public Conexion()
        {
            DBConnection = new OracleConnection(Credentials);
            DBConnection.Open();
        }

        public OracleConnection GetConnection() => DBConnection;

        public void CloseConnection()
        {
            DBConnection.Close();
            DBConnection.Dispose();
        }
    }
}