﻿using Facturas.Negocio;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Facturas.Datos
{
    public class DAOVenta
    {
        protected Conexion Conexion;
        protected OracleCommand Comando;

        public bool ExisteVenta(int CodigoVenta)
        {
            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select count(codigo_venta) from venta where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            int Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConnection();

            return Count > 0;
        }

        public Venta GetVenta(int CodigoVenta)
        {
            if (!ExisteVenta(CodigoVenta)) { return new Venta(); }

            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select fecha_venta, fecha_venta, tipo_documento from venta where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            OracleDataReader Reader = Comando.ExecuteReader();

            Venta Result = new Venta();
            while (Reader.Read())
            {
                Result = new Venta()
                {
                    CodigoVenta = CodigoVenta,
                    Fecha = Convert.ToDateTime(Reader["fecha_venta"]),
                    Sucursal = Reader["fecha_venta"].ToString(),
                    TipoDocumento = Reader["tipo_documento"].ToString(),
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConnection();

            return Result;
        }
    }
}