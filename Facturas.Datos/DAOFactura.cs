﻿using Facturas.Negocio;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Facturas.Datos
{
    public class DAOFactura
    {
        protected Conexion Conexion;
        protected OracleCommand Comando;

        public bool ExisteFactura(int CodigoVenta)
        {
            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select count(codigo_venta) from factura where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            int Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConnection();

            return Count > 0;
        }

        public Factura GetFactura(int CodigoVenta)
        {
            if (!ExisteFactura(CodigoVenta)) { return new Factura(); }

            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select valor_neto_factura, iva_factura, valor_total_factura, codigo_cliente from factura where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            OracleDataReader Reader = Comando.ExecuteReader();

            Factura Result = new Factura();
            while (Reader.Read())
            {
                Result = new Factura()
                {
                    CodigoVenta = CodigoVenta,
                    ValorNeto = Convert.ToInt32(Reader["valor_neto_factura"]),
                    ImpuestoIva = Convert.ToInt32(Reader["iva_factura"]),
                    ValorTotal = Convert.ToInt32(Reader["valor_total_factura"]),
                    CodigoCliente = Convert.ToInt32(Reader["codigo_cliente"]),
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConnection();

            return Result;
        }
    }
}