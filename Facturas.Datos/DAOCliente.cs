﻿using Facturas.Negocio;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Facturas.Datos
{
    public class DAOCliente
    {
        protected Conexion Conexion;
        protected OracleCommand Comando;

        public bool ExisteCliente(int CodigoCliente)
        {
            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select count(codigo_cliente) from cliente where codigo_cliente = :codigo_cliente"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_cliente", CodigoCliente));
            int Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConnection();

            return Count > 0;
        }

        public Cliente GetCliente(int CodigoCliente)
        {
            if (!ExisteCliente(CodigoCliente)) { return new Cliente(); }

            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select rut_cliente, nombre_cliente, direccion_cliente, ciudad_cliente, comuna_cliente, telefono_cliente from cliente where codigo_cliente = :codigo_cliente"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_cliente", CodigoCliente));
            OracleDataReader Reader = Comando.ExecuteReader();

            Cliente Result = new Cliente();
            while (Reader.Read())
            {
                Result = new Cliente()
                {
                    Codigo = CodigoCliente,
                    Rut = Reader["rut_cliente"].ToString(),
                    Nombre = Reader["nombre_cliente"].ToString(),
                    Direccion = Reader["direccion_cliente"].ToString(),
                    Ciudad = Reader["ciudad_cliente"].ToString(),
                    Comuna = Reader["comuna_cliente"].ToString(),
                    Telefono = Reader["telefono_cliente"].ToString(),
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConnection();

            return Result;
        }
    }
}