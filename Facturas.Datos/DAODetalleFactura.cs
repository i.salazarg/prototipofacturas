﻿using Facturas.Negocio;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Facturas.Datos
{
    public class DAODetalleFactura
    {
        protected Conexion Conexion;
        protected OracleCommand Comando;

        public bool ExisteDetalle(int CodigoVenta)
        {
            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select count(codigo_venta) from detallefactura where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            int Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConnection();

            return Count > 0;
        }

        public DetalleFactura GetDetalle(int CodigoVenta)
        {
            if (!ExisteDetalle(CodigoVenta)) { return new DetalleFactura(); }

            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select nombre_ejemplar, cantidad_detalle, valor_detalle from detallefactura where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            OracleDataReader Reader = Comando.ExecuteReader();

            DetalleFactura Result = new DetalleFactura();
            while (Reader.Read())
            {
                Result = new DetalleFactura()
                {
                    CodigoVenta = CodigoVenta,
                    NombreEjemplar = Reader["nombre_ejemplar"].ToString(),
                    Cantidad = Convert.ToInt32(Reader["cantidad_detalle"]),
                    Valor = Convert.ToInt32(Reader["valor_detalle"]),
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConnection();

            return Result;
        }
    }
}