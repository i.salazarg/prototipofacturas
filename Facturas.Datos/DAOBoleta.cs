﻿using Facturas.Negocio;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Facturas.Datos
{
    public class DAOBoleta
    {
        protected Conexion Conexion;
        protected OracleCommand Comando;

        public bool ExisteBoleta(int CodigoVenta)
        {
            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select count(codigo_venta) from boleta where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            int Count = Convert.ToInt32(Comando.ExecuteScalar());

            Comando.Dispose();
            Conexion.CloseConnection();

            return Count > 0;
        }

        public Boleta GetBoleta(int CodigoVenta)
        {
            if (!ExisteBoleta(CodigoVenta)) { return new Boleta(); }

            Conexion = new Conexion();
            Comando = new OracleCommand
            {
                Connection = Conexion.GetConnection(),
                CommandText = "select codigo_boleta, precio_boleta from boleta where codigo_venta = :codigo_venta"
            };
            Comando.Parameters.Add(new OracleParameter("codigo_venta", CodigoVenta));
            OracleDataReader Reader = Comando.ExecuteReader();

            Boleta Result = new Boleta();
            while (Reader.Read())
            {
                Result = new Boleta()
                {
                    CodigoVenta = CodigoVenta,
                    CodigoBoleta = Convert.ToInt32(Reader["codigo_boleta"]),
                    Precio = Convert.ToInt32(Reader["precio_boleta"])
                };
            }

            Reader.Close();
            Reader.Dispose();
            Comando.Dispose();
            Conexion.CloseConnection();

            return Result;
        }
    }
}